import * as assert from "assert"
import { my_array_alpha } from "../day-1/exercise-5.js"

describe('Exo 5', function () {
    describe('my_array_alpha()', function () {
        it('should return an array with char of the string', function () {
            assert.deepEqual(my_array_alpha("test"), ['t','e','s','t'])
        });
        it('should return 0', function () {
            assert.equal(my_array_alpha(12), 0)
        });
    });
});