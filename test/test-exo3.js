import * as assert from "assert"
import { my_alpha_number } from "../day-1/exercise-3.js"

describe('Exo 3', function () {
    describe('my_alpha_number()', function () {
        it('should return a string of the number', function () {
            assert.equal(my_alpha_number(8),"8")
        });
        it('should return a empty string', function () {
            assert.equal(my_alpha_number("8"),"")
        });
    });
});