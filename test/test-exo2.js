import * as assert from "assert"
import { my_display_alpha_reverse } from "../day-1/exercise-2.js"

describe('Exo 1', function () {
    describe('my_display_alpha_reverse()', function () {
        it('should return the alphabet', function () {
            assert.equal(my_display_alpha_reverse("abcdefghijklmopqrstuvwxyz"),"zyxwvutsrqponmlkjlhgfedcba")
        });
    });
});