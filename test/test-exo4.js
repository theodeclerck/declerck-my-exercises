import * as assert from "assert"
import { my_size_alpha } from "../day-1/exercise-4.js"

describe('Exo 4', function () {
    describe('my_size_alpha()', function () {
        it('should return the number of char of the string', function () {
            assert.equal(my_size_alpha("test"), 4)
        });
        it('should return 0', function () {
            assert.equal(my_size_alpha(12), 0)
        });
    });
});