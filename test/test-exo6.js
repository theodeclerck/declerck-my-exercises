import * as assert from "assert"
import { my_length_array } from "../day-1/exercise-6.js"

describe('Exo 6', function () {
    describe('my_length_array()', function () {
        it('should return the length of the array', function () {
            assert.equal(my_length_array(['t','e','s','t']), 4)
        });
    });
});